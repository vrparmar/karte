package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

// SmartImz ...
type SmartImz struct{}

// Immunization ...
type Immunization struct {
	Person      string
	Location    string
	Date        string
	Type        string
	Dose        string
	Instruction string
}

// Init ...
func (s *SmartImz) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

// Invoke ...
func (s *SmartImz) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	fn, args := stub.GetFunctionAndParameters()

	var result string
	var err error
	switch fn {
	case "add":
		result, err = s.add(stub, args)
	default:
		result, err = s.query(stub, args)
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte(result))
}

func (s *SmartImz) add(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 6 {
		return "", errors.New("Incorrect number of arguments. Expecting 6")
	}

	i := Immunization{}
	i.Person = args[0]
	i.Location = args[1]
	i.Date = args[2]
	i.Type = args[3]
	i.Dose = args[4]
	i.Instruction = args[5]

	immJSONBytes, err := json.Marshal(&i)
	if err != nil {
		return "", err
	}

	if err := stub.PutState(args[0], immJSONBytes); err != nil {
		return "", err
	}
	return args[0], nil
}

func (s *SmartImz) query(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", errors.New("Incorrect number of arguments. Expecting 1")
	}

	person := args[0]

	results, err := stub.GetHistoryForKey(person)
	if err != nil {
		return "", err
	}
	defer results.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for results.HasNext() {
		response, err := results.Next()
		if err != nil {
			return "", err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")
		// if it was a delete operation on given key, then we need to set the
		//corresponding value null. Else, we will write the response.Value
		//as-is (as the Value itself a JSON marble)
		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString("\"")
		buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		buffer.WriteString("\"")

		buffer.WriteString(", \"IsDelete\":")
		buffer.WriteString("\"")
		buffer.WriteString(strconv.FormatBool(response.IsDelete))
		buffer.WriteString("\"")

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	return buffer.String(), nil
}

func main() {
	if err := shim.Start(new(SmartImz)); err != nil {
		fmt.Printf("Error starting Smart Immunization Contract")
	}
}
